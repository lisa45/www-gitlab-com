---
layout: job_family_page
title: Legal Counsel
---
 
The Legal Counsel job family is responsible for advising clients across GitLab on legal matters related to their functional area of expertise.
 
## Levels
 
### Legal Counsel (Intermediate)
 
The Legal Counsel (Intermediate) reports to the [Director, Legal](/job-families/legal/director-legal-us/).
 
#### Legal Counsel (Intermediate) Job Grade

The Legal Counsel is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades)
 
#### Legal Counsel (Intermediate) Responsibilities

* Partner with team members and stakeholders across GitLab to provide pragmatic advice to minimize legal risks while advancing the goals of the business
* Advise on, draft, maintain and review templates and policies
* Monitor and analyze changes in the law and legal risks; propose creative and effective solutions to address those risks
* Review, draft and negotiate agreements
* Build the necessary processes, systems, and documentation to improve legal department processes and resources
 
 #### Legal Counsel (Intermediate) Requirements

* 3 - 5 years' legal experience, preferably in a multinational company
* JD/LLB or local law degree equivalent and admission to local governing body
* Experience working effectively across business units and internal functions to efficiently resolve business issues
* Practical yet creative problem-solving approach that emphasizes addressing business needs while protecting GitLab’s interests
* Proactive, dynamic, and result driven self starter with strong attention to detail
* Familiarity working across the globe, to support multiple time zones and cultures
* Outstanding interpersonal skills, the ability to interface effectively with all business functions throughout the organization with a highly responsive and service-oriented attitude
* Sound and practical business judgment
* Previous experience in a remote work environment would be an added advantage
* Ability to use GitLab
 
## Specialties

#### Product and Privacy

The Legal Counsel, Product and Privacy collaborates with clients across GitLab on a broad range of matters including IP, product, marketing and privacy.  In addition to the general Responsibilities and and Requirements above, this role specifically requires:
* Experience providing counseling to product and engineering teams on products and features throughout the development lifecycle, and identifying legal issues
* Knowledge and understanding of open source licensing
* Experience working on advertising, trademark, marketing issues (email campaigns, sweepstakes, promotional activities), including familiarity with FTC regulations, GDPR and other local law requirements
* Experience identifying privacy issues, conducting privacy impact assessments, and advising on privacy compliance requirements(e.g., GDPR, CCPA)
* A strong mix of legal, technical, and business acumen that will allow you to understand and discuss complex technical issues and data flows.
* Basic knowledge of DMCA and Law Enforcement requirements
* Experience working in a SaaS and self-hosted software company

#### Employment - EMEA

The Legal Counsel, Employment - EMEA collaborates with clients across GitLab on a broad range of matters across the entire arc of the employment relationship, from recruiting to onboarding, compensation, performance management, leave management, separation, and, where necessary, litigation. In addition to the general Responsibilities and Requirements above, this role specifically requires:
* Experience providing counseling to people business partners and team member relations teammates regarding team member relations concerns.
* Knowledge and understanding of general employment compliance requirements in the EMEA region with an ability to flag and seek out from local counsel specific local requirements as applicable.
* Experience drafting employment policies, especially those that cross multiple jurisdictions in the EMEA region.
* Familiarity with GDPR and other local privacy law requirements as it relates to employee privacy.
* A strong mix of legal, technical, and business acumen that will allow you to understand and discuss complex employment issues.
* Experience managing employment litigation, appeals tribunals, agency investigations, and/or negotiating separation agreements.
* Ability to provide support, including issue spotting and managing outside counsel, in regions outside of EMEA, on an as-needed basis.

### Performance Indicators
 
#### Legal Counsel (Privacy and Product)

* Legal oversight of product development and understanding of the GitLab products and features 
* Development of standard processes, trainings and practices for marketing issues
* Effective Open Source License management and counseling

#### Legal Counsel (Employment - EMEA)

* Legal oversight of employment compliance and understanding of the applicable employment solutions in place across jurisdictions.
* Development of standard policies, processes, trainings and practices for employment issues.
* Strategic business partner for People Success group.
 
## Career Ladder
 
The next step in the Legal Counsel job family is not yet defined at GitLab.
 
## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).
 
* Selected candidates will be invited to schedule a 30-minute screening call with our Global Recruiters
* Next, candidates will be invited to schedule an interview with our Director of Legal, Privacy and Product
* Next, candidates will be invited to schedule with additional team members of the legal department
*Next, candidates will be invited to schedule an interview with our Chief Legal Officer
* Finally, candidates may be invited to schedule interviews with Directors or team members in relevant functional areas
 
Additional details about our process can be found on our [hiring page](/handbook/hiring).
